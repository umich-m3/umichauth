<?php
/**
 * Created by PhpStorm.
 * User: mlhess
 * Date: 8/7/15
 * Time: 3:25 PM
 */


function umichauth_admin_settings($form,$form_state) {
$form = array();
$form['umichauthurl'] = array(
  '#type'=>'textfield',
  '#title' => 'URL to hosting auth server',
  '#default_value' => variable_get('umichauthurl')
);
  $options = array();
  $options['Y'] = t('Yes');
  $options['N'] = t('No');

  $form['umichauthuser'] = array(
    '#type'=>'radios',
    '#title' => 'Replace Drupal login',
    '#options' => $options,
    '#default_value' => variable_get('umichauthuser','N')
  );
    $form['umichauthcreate'] = array(
    '#type'=>'radios',
    '#title' => 'Create account if there is not one',
    '#options' => $options,
    '#default_value' => variable_get('umichauthcreate','N')
  );
    $form['umichauthlimitdept'] = array(
    '#type'=>'textfield',
    '#title' => 'You can limit users by dept group, please provide a comma separated
    of dept groups',
    '#default_value' => variable_get('umichauthlimitdept','')
  );
    $form['umichauthlimitvp'] = array(
    '#type'=>'textfield',
    '#title' => 'You can limit users by vp group, please provide a comma separated
    of vp groups.  VP groups look like EXEC_VP_MED_AFF',
    '#default_value' => variable_get('umichauthlimitvp','')
  );
  
    
  $form['text'] = array(
    '#markup'=>'After you edit changes here, please clear all caches'
  );
  global $conf;
  $key = @$conf['jwtkey'];
  if (!isset($key)) {
    $form['text2'] = array(
      '#markup'=>'<h1>Warning, this must be setup by uofmhosting before you use it.  If you enable it, you will be locked out of your site.  Contact uofmhosting@umich.edu</h1>'
    );
  }
return system_settings_form($form);
}